// Задание
// Реализовать переключение вкладок (табы) на чистом Javascript.
//
//     Технические требования:
//
//     В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку
// отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть
// скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут
// добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте,
// из-за таких правок не переставала работать.

let tab = function () {
    let tabLink = document.querySelectorAll('.tabs-title'),
        tabContent = document.querySelectorAll('.tab'), tabName;
    tabLink.forEach(item => {
        item.addEventListener('click', selectTabLink)
    });

    function selectTabLink() {
        tabLink.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab');
        selectTabText(tabName);
    }

    function selectTabText(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
        })
    }
};
tab();