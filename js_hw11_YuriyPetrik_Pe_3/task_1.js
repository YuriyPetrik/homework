
//Теоретический вопрос
// Почему для работы с input не рекомендуется использовать события клавиатуры?

// Потому что на многих современных устройствах есть и другие способы «ввести что-то».
// Например, распознавание речи (это особенно актуально на мобильных устройствах) или Копировать/Вставить с помощью мыши.
//Поэтому, если мы хотим корректно отслеживать ввод в поле <input>, то одних клавиатурных событий недостаточно.

    const [...b] = document.getElementsByClassName("btn");

document.addEventListener("keydown", (ev) => {
    b.forEach(element => element.style.backgroundColor = "#000");

    const letter = [...ev.code].splice(3);

    b.find(value => {
        if (value.textContent === letter[0] || value.textContent === ev.key)
            value.style.backgroundColor = "blue";
    });
});