const buttonEnter = document.getElementById("i");
const buttonConfirm = document.getElementById("ii");
buttonEnter.onclick = showEnter;
buttonConfirm.onclick = showConfirm;
let inputEnter = document.getElementById("show-enter");
let inputConfirm = document.getElementById("show-confirm");
const iconEnter = document.getElementById("i");
const iconConfirm = document.getElementById("ii");

function showEnter() {
    if (inputEnter.getAttribute('type') === 'password') {
        inputEnter.removeAttribute('type');
        inputEnter.setAttribute('type', 'text');
        iconEnter.className = 'far fa-eye-slash';
    } else {
        inputEnter.removeAttribute('type');
        inputEnter.setAttribute('type', 'password');
        iconEnter.className = 'far fa-eye';
    }
}

function showConfirm() {
    if (inputConfirm.getAttribute('type') === 'password') {
        inputConfirm.removeAttribute('type');
        inputConfirm.setAttribute('type', 'text');
        iconConfirm.className = 'far fa-eye-slash';
    } else {
        inputConfirm.removeAttribute('type');
        inputConfirm.setAttribute('type', 'password');
        iconConfirm.className = 'far fa-eye';
    }
}


document.getElementById("cancel").addEventListener("click", function () {
    if (document.getElementById("show-enter").value == document.getElementById("show-confirm").value) {
        alert("You are welcome");
    } else {
        showError();
    }

});

function showError() {
    document.querySelector(".error").classList.remove("d-none");
};
