const checkTheme = () => {
    if (localStorage.getItem('theme') === 'true') {
        appContainer.classList.add('app--night');
        input.checked = true;
    }
};

const appContainer = document.querySelector('.app');
const switcher = appContainer.querySelector('.switch');
const input = switcher.querySelector('.check');

checkTheme();

switcher.addEventListener('click', (e) => {
    const elem = e.target.closest('input');
    if (elem) {
        localStorage.setItem('theme', elem.checked);
        appContainer.classList.toggle('app--night');
    }
});
