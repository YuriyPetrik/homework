// Теоретический вопрос
//
// Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

// var - переменная, которая использовалась до переменных let и const и она не ограничена блоком.
// let - переменная, которая ограничена блоком и ее можно переназначить.
// const - переменная, которая также ограничена блоком и ее нельзя переназначить.

// Почему объявлять переменную через var считается плохим тоном?

// var считается устаревшей и согласно новым спецификациям нужно использывать let и const,
// также var игнорирует блоки, мы получаем глобальную переменную.

let userName = prompt("What's your name?");
while (!userName) {
    userName = prompt("What's your name?");
}
let userAge = +prompt("How old are you? ");
while (!userAge) {
    userAge = +prompt("How old are you? ")
}
if (userAge < 18) {
    document.write("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        document.write(`Welcome ${userName}`);
    } else document.write('You are not allowed to visit this website');
} else {
    document.write(`Welcome ${userName}`);
}
