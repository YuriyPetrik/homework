// Теоретический вопрос
//
// Опишите своими словами, как Вы понимаете, что такое обработчик событий.
//Наступает какое-то события: пользователь кликает мышью, нажимает какие-то клавишы на клавиатуре...
// и после этого события что-то происходит. Событию можно назначить обработчик(функцию)
//
//
//     Задание
// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет
//     служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст:
//     Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X).
//     Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой,
//     под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.
// document.getElementById('price-item').focus(<style>background color</style>);

function createPriceItem(task) {
    document.querySelector("ul").innerHTML = `<span class="list-item-text">Текущая цена: ${task} грн.</span>`;
    document.querySelector("span").append(createRemoveBtn());
    document.querySelector("span").className = "list-item";
}

function addPriceItem() {
    let inputValue = document.querySelector(".input").value;
    if (isValidValue(inputValue)) {
        hideError();
        createPriceItem(inputValue);
        document.getElementById("input").value = "";
    } else {
        showError();
    }
}

let addPriceItemBtn = document.getElementById("addItem");
addPriceItemBtn.addEventListener("click", function () {
    addPriceItem();

});

function isValidValue(value) {
    if (value === "" || value < 0 || isNaN(value)) {
        return false;
    }
    return true;

}

function showError() {
    document.querySelector(".error").classList.remove("d-none");
    document.getElementById("input").value = "";
}

function hideError() {
    document.querySelector(".error").classList.add("d-none");
}

function createRemoveBtn() {
    let newSpan = document.createElement("span");
    newSpan.innerHTML = "x";
    newSpan.className = "list-item-remove-btn";
    newSpan.addEventListener("click", function (event) {
        event.target.closest(".list-item").remove();
    });
    return newSpan;
}

