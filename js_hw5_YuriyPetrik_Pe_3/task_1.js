function CreateNewUser(name, lastName, birthday) {
    return {
        firstName: name,
        lastName: lastName,
        birthday: birthday,
        getPassword: function () {
            return name[0].toUpperCase() + lastName.toLowerCase() + birthday.slice(6);
        },
        getAge: function () {
            return 2021 - birthday.slice(6);
        },
    }
}
const newUser = CreateNewUser(prompt("Ваше имя"), prompt("Ваша фамилия"), prompt("Число, месяц, год рождения"));

console.log(CreateNewUser());
console.log(newUser.getPassword());
console.log(newUser.getAge());
