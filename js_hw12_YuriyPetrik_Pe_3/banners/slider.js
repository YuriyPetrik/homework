// #Теоретические вопросы:
//
// 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
// 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
// 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?

// Ответы:
// 1. setTimeout выполняет функцию один раз во время тайм-аута. setInterval выполняет функцию многократно и с интервалом.
// 2. Если этот параметр будет 0, что означает выполнить "immediately", или, точнее, как можно скорее.
// 3. "clearInterval()" важно вызывать потому, что  "clearInterval()"  очищает ОСТАНАВЛИВАЕТ функцию `setInterval()`.

const slides = document.querySelectorAll('#slides .slide');
let currentSlide = 0;
const stop = document.getElementById('stop');
stop.addEventListener('click', sliderStop);
const play = document.getElementById('play');
play.addEventListener('click', sliderPlay);

let start = setInterval(nextSlide, 3000);
let showing = true;

function nextSlide() {
    slides[currentSlide].className = 'slide';
    currentSlide = (currentSlide + 1) % slides.length;
    slides[currentSlide].className = 'slide showing';
}

function sliderStop() {
    clearInterval(start);
    showing = false
}

function sliderPlay() {
    if (!showing) {
        start = setInterval(nextSlide, 3000);
        showing = true
    }
}